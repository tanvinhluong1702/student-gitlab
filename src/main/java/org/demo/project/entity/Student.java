package org.demo.project.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Student {
    private Long id;

    private String name;
    private String className;
    private int age;
    private String address;
    private int englishScore;
    private int literatureScore;
    private int mathScore;
    private int physicsScore;
    private int chemistryScore;
    public Student() {
    }

    public Student(String name, String className, int age, String address, int englishScore, int literatureScore, int mathScore, int physicsScore, int chemistryScore) {
        this.name = name;
        this.className = className;
        this.age = age;
        this.address = address;
        this.englishScore = englishScore;
        this.literatureScore = literatureScore;
        this.mathScore = mathScore;
        this.physicsScore = physicsScore;
        this.chemistryScore = chemistryScore;
    }
}
